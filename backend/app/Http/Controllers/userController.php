<?php

namespace App\Http\Controllers;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class userController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users|string',
            'password' => 'required|string|min:6',
            'name' => 'required|string',

        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()], 409);
        }
        $p = new User();
        $p->name = $request->name;
        $p->email = $request->email;
        $p->password = encrypt($request->password);

        $p->save();
        return response()->json(['message' => 'User has been Registered'], 200);
    }
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required|string',


        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()], 401);
        }

        $user = User::where('email', $request->email)->get()->first();
        if (!$user) {
            return response()->json(['error' => 'user not found'], 401);
        }
        $password = decrypt($user->password);
        if ($password != $request->password) {
            return response()->json(['error' => 'password not match'], 401);
        }

        $tokenResult = $user->createToken('Personal Access TOken');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json(['data' => [
            'user' => $user,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expire_at)->toDateTimeString()
        ]]);
    }
}
