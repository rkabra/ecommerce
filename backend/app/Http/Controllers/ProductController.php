<?php

namespace App\Http\Controllers;

use App\Models\product as ModelsProduct;
use App\Models\product;
use Dotenv\Dotenv;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function add(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'category'=>'required',
            'brand'=>'required',
            'desc'=>'required',
            'image'=>'required|image',
            'price'=>'required',
        ]);
        if($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()->all()], 409);
        }
        $p=new Product();
        $p->name=$request->name;
        $p->category=$request->category;
        $p->brand=$request->brand;
        $p->desc=$request->desc;
        $p->price=$request->image; 
        $p->save();
            $url="http://localhost:8000/storage/";
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $path = $request->file('image')->storeAs('proimage/', $p->id.'.'.$extension);
            $p->image=$path;
            $p->imgpath=$url.$path;
            $p->save();

        
    }
    public function update(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'category'=>'required',
            'brand'=>'required',
            'id'=>'required',
            'desc'=>'required',
            
        ]);
        if($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()->all()], 409);
        }
        $p= Product::find($request->id);
        $p->name=$request->name;
        $p->category=$request->category;
        $p->brand=$request->brand;
        $p->desc=$request->desc;
        $p->price=$request->image; 
        $p->save();
        return response()->json(['message'=>"product Successfully Update"]);
        
    }
    public function delete(Request $request)
    {
        $validator=Validator::make($request->all(),[
            
            'id'=>'required', 
        ]);
        if($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()->all()], 409);
        }
        $p= Product::find($request->id)->delete();
        
        return response()->json(['message'=>"product Successfully Delete"]);
        
    }
    public function show(Request $request)
    {
        session(['key'=> $request->keys]);
        $products=product::where(function ($q){
            $q->where('product.id','LIKE','%'.session('keys').'%')
                ->orwhere('product.name','LIKE','%'.session('keys').'%')
                ->orwhere('product.price','LIKE','%'.session('keys').'%')
                ->orwhere('product.category','LIKE','%'.session('keys').'%')
                ->orwhere('product.brand','LIKE','%'.session('keys').'%');  
        })->select('products.*')->get();
        return response()->json((['products'=>$products]));
    }
}
