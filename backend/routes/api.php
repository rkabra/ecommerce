<?php

use App\Models\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\userController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {


    Route::post('login', [userController::class, 'login'])->name('login');
    Route::post('register', [userController::class, 'register'])->name('register');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('add', [ProductController::class, 'add'])->name('add');
Route::post('update', [ProductController::class, 'update'])->name('update');
Route::post('delete', [ProductController::class, 'delete'])->name('delete');
Route::post('show', [ProductController::class, 'show'])->name('show');
